module.exports = function (grunt) {

    var package = grunt.file.readJSON('package.json');

    grunt.initConfig({
        // Import JS
        import_js: {
            files: {
                expand: true,
                cwd: 'js/src',
                src: ['app.js'],
                dest: 'js/build/',
                ext: '.js'
            }
        },

        // Minify JS
        uglify: {
            main: {
                options: {
                    beautify: package.devMode
                },
                files: [
                    {
                        expand: true,
                        cwd: 'js/build/',
                        src: 'app.js',
                        dest: 'js/dist/',
                        rename: function (dest, matchedSrcPath, options) {
                            // return the destination path and filename:
                            return (dest + matchedSrcPath).replace('.js', '.min.js');
                        }
                    }
                ]
            }
        },


        /**
         * Styles
         */

        // Compile SCSS into CSS
        sass: {
            main: {
                files: {
                    'css/build/style.css': 'css/sass/style.scss'
                }
            }
        },

        // Clean map files
        clean: {
            scss: [
                'css/build/style.css.map'
            ]
        },

        // Minify CSS
        cssmin: {
            main: {
                options: {
                    shorthandCompacting: false,
                    roundingPrecision: -1
                },
                files: [
                    {
                        expand: true,
                        cwd: 'css/build/',
                        src: '*.css',
                        dest: 'css/dist/',
                        rename: function (dest, matchedSrcPath, options) {
                            // return the destination path and filename:
                            return (dest + matchedSrcPath).replace('.css', '.min.css');
                        }
                    }
                ]
            }
        },

        // No minify on files if it's dev environment
        copy: {
            js: {
                src: 'js/build/main.js',
                dest: 'js/dist/main.min.js'
            },
            css: {
                src: 'css/build/style.css',
                dest: 'css/dist/style.min.css'
            }
        },


        /**
         * Watch
         */

        // watch changes
        watch: {
            js: {
                files: [
                    'js/src/*.js'
                ],
                tasks: ['import_js', 'uglify']
            },
            css: {
                files: [
                    'css/sass/*.scss'
                ],
                tasks: ['sass:main', ((package.devMode === true) ? 'copy:css' : 'cssmin:main'), 'clean']
            }
        }
    });

    //register script operations
    grunt.loadNpmTasks('grunt-import-js');
    grunt.loadNpmTasks('grunt-contrib-uglify');

    //register stylesheet operations
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-clean');

    //register watching
    grunt.loadNpmTasks('grunt-contrib-watch');

    //start
    if (package.devMode === true) {
        grunt.registerTask('default', ['import_js', 'uglify', 'sass', 'copy', 'clean', 'watch']);
    } else {
        grunt.registerTask('default', ['import_js', 'uglify', 'sass', 'cssmin', 'clean']);
    }
};