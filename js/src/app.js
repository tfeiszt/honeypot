 // @import "/controls.js";
 // @import "/validators.js";
 // @import "/controller.js";

/**
 * Application
 * ES5 test
 */
'use strict'

var app = app || {}
/**
 * Options
 * @type {{debug: boolean}}
 */
app.options = {
    'debug': false
};
/**
 * Returns container <div>
 * @returns {Element}
 */
app.getContainer = function() {
    return document.getElementById('honeypot');
}
/**
 * Create controller
 * @type {Controller}
 */
app.controller = new Controller(app.options);
/**
 * Set up validation rules
 */
app.controller.setRules([
    {'type': 'required', 'message': 'Field is mandatory', 'params': []},
    {'type': 'integer', 'message': 'It must be a number', 'params': []},
    {'type': 'minValue', 'message': 'Minimum value is 0', 'params': [0]},
    {'type': 'maxValue', 'message': 'Maximum value is 10k', 'params': [10000]}
]);
/**
 * Set up a general view object
 */
app.controller.setView({
    /**
     * Rendering following html elements:
     * label for caption
     * texbox
     * button
     * label for error messages
     */
    render: function() {
        app.getContainer().appendChild(
            (new Label({
                'caption': 'Number of bees: ',
                'class': '',
                'debug': app.options.debug
                })
            ).template()
        ).parentNode.appendChild(
            (new Input({
                'debug': app.options.debug
                })
            ).template()
        ).parentNode.appendChild(
            (new Button({
                    'onClick': app.controller.doApplicationLogic,
                    'debug': app.options.debug
                })
            ).template()
        ).parentNode.appendChild(
            (new Label({
                    'caption': '',
                    'class': 'hidden',
                    'debug': app.options.debug
                })
            ).template()
        );
    },

    /**
     * Shows an error message
     * @param message
     */
    showError: function(message) {
        app.getContainer().lastElementChild.innerHTML = message;
        app.getContainer().lastElementChild.className = 'error';
    },

    /**
     * Hide error message
     */
    hideError: function() {
        app.getContainer().lastElementChild.innerHTML = '';
        app.getContainer().lastElementChild.className = 'hidden';
    }
});
/**
 * Custom callback on button click
 */
app.controller.setCallback(function(value){
    var v = parseInt(value);
    for(var i = 0; i < v; i++) {
        var delay = 20 * i;
        setTimeout(app.controller.log, delay, i);
    }
    setTimeout(app.controller.log, (20 * v), value + ' bee(s) in the honeypot');
});
/**
 * Render application view
 */
app.controller.view.render();
