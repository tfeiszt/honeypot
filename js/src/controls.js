/**
 * Html control classes
 */
'use strict'

/**
 * Abstract control
 * @param options
 * @constructor
 */
function AbstractControl(options) {
    this.options = (options) ? options : {};
    this.type = 'undefined';
    /**
     * Returns a value of options object, or default
     * @param name
     * @param defaultValue
     * @returns {*}
     */
    this.getOption = function(name, defaultValue) {
        return (this.options[name] && typeof this.options[name] != 'undefined') ? this.options[name] : defaultValue;
    }
}

/**
 * General event, does not create anything but logs in debug mode
 */
AbstractControl.prototype.template = function() {
    if (this.getOption('debug', false) === true) {
        console.log(this.type + ' has been created');
    }
}

/**
 * Label
 * @param options
 * @constructor
 */
function Label(options) {
    AbstractControl.call(this, options);

    this.type = 'label';
}
/**
 * Label inherits AbstractControl, but we override template method to make a label
 * @type {Label}
 */
Label.prototype = Object.create(AbstractControl.prototype);
Label.prototype.template = function () {
    var control = document.createElement('label');
    control.innerHTML = this.getOption.call(this, 'caption', '');
    control.className = this.getOption.call(this, 'class', '');
    AbstractControl.prototype.template.call(this);
    return  control;
}

/**
 * Input
 * @param options
 * @constructor
 */
function Input(options) {
    AbstractControl.call(this, options);

    this.type = 'input';
}
/**
 * Input inherits AbstractControl, but we override template method to make a textbox
 * @type {Input}
 */
Input.prototype = Object.create(AbstractControl.prototype);
Input.prototype.template = function () {
    var control = document.createElement('input');
    control.className = this.getOption.call(this, 'class', '');
    AbstractControl.prototype.template.call(this);
    return  control;
}

/**
 * Button
 * @param options
 * @constructor
 */
function Button(options) {
    AbstractControl.call(this, options);

    this.type = 'button';
}
/**
 * Button inherits AbstractControl, but we override template method to make a button
 * @type {Button}
 */
Button.prototype = Object.create(AbstractControl.prototype);
Button.prototype.template = function () {
    var control = document.createElement('button');
    control.innerHTML = this.getOption.call(this, 'caption', 'Send');
    control.className = this.getOption.call(this, 'class', '');
    //Custom click event, or a default one
    control.addEventListener('click', this.getOption.call(this, 'onClick', function(event) {
        console.log('click');
    }));
    AbstractControl.prototype.template.call(this);
    return  control;
}
