/**
 * Controller class
 */
'use strict'

/**
 * General controller
 * It controls a view, has an input textbox and a button.
 * Handles custom validation on input, and custom callback using valid input.
 * @param options
 * @constructor
 */
function Controller(options) {
    this.options = (options && typeof options != 'undefined') ? options : {}; //custom options of behaviour
    this.view = null; //custom view object (object)
    this.rules = []; //validation rules (object array)
    this.validationMessages = []; //validation error messages (string array)
    this.callback = null; //custom callback (callable)
    this.getOption = this.getOption.bind(this);
    this.log = this.log.bind(this);
    this.setView = this.setView.bind(this);
    this.setCallback = this.setCallback.bind(this);
    this.setRules = this.setRules.bind(this);
    this.validate = this.validate.bind(this);
    this.doApplicationLogic = this.doApplicationLogic.bind(this);
}
/**
 * Returns a value of options, or default
 * @param name
 * @param defaultValue
 * @returns {*}
 */
Controller.prototype.getOption = function(name, defaultValue) {
    return (this.options[name] && typeof this.options[name] != 'undefined') ? this.options[name] : defaultValue;
}

/**
 * Output to console
 * @param message
 */
Controller.prototype.log = function(message) {
    console.log(message);
}

/**
 * Delegates a view
 * @param view
 * @returns {Controller}
 */
Controller.prototype.setView = function (view) {
    this.view = view;
    return this;
}

/**
 * Delegates a callback
 * @param callback
 * @returns {Controller}
 */
Controller.prototype.setCallback = function(callback) {
    this.callback = callback;
    return this;
}

/**
 * Delegates a rule array
 * @param rules
 * @returns {Controller}
 */
Controller.prototype.setRules = function(rules) {
    this.rules = rules;
    return this;
}

/**
 * Validation
 * It creates a ValidationFactory instance which handles registered validation processes.
 * @param value
 * @returns {boolean}
 */
Controller.prototype.validate = function(value) {
    var self = this;
    var vFactory = new ValidatorFactory({
        'debug' : this.getOption('debug', false)
    });
    this.validationMessages = [];
    this.rules.every(function(item){

        //creates a Validator instance by type
        var validator = vFactory.getValidator(item.type);

        //runs validation on value, and optionally compares to expected values
        if (! validator.validate(value, item.params)) {
            self.validationMessages.push(item.message);
            return false;  // Terminate validation at first failure
        } else {
            return true;
        }
    });
    return (this.validationMessages.length === 0) || false;
}

/**
 * General application logic: Run validation, if passed, run custom callback.
 * @param event
 */
Controller.prototype.doApplicationLogic = function(event) {
    // get input value
    // Honestly that would be nice if this value could be comes via an onChange event listener...
    var value = (event.target.parentNode.getElementsByTagName('input')[0]).value;
    // As view is an object all callable on that must be tested.
    // It would be also nice to use interface which is not supported in native ES5 (as my knowledge)
    if (typeof(this.view.hideError) == "function") {
        this.view.hideError();
    }
    if (! this.validate(value)) {
        //Same check like above.
        if (typeof(this.view.showError) == "function") {
            //show validation message
            this.view.showError(this.validationMessages.join('<br>'));
        } else {
            this.log(this.validationMessages.join(', '));
        }
    } else {
        //Passed, do custom application logic
        this.callback(value);
    }
}
