/**
 * Validator classes
 */
'use strict'

/**
 * ValidatorFactory
 * This class is able to create different kind of validator classes
 * @param options
 * @constructor
 */
function ValidatorFactory(options) {

    this.options = (options && typeof options != 'undefined') ? options : {};

    /**
     * Returns new validator instances
     * @param type
     * @returns {*}
     */
    this.getValidator = function( type ) {
        var self = this;
        var validator = null;
        //Registered validator classes
        //It would be nice to do dynamically by validatorClass.type or class name. Not big issue.
        switch (type) {
            case 'required':
                validator = new RequiredValidator(self.getOption('debug', false));
                break;
            case 'integer':
                validator = new IntegerValidator(self.getOption('debug', false));
                break;
            case 'minValue':
                validator = new MinValueValidator(self.getOption('debug', false));
                break;
            case 'maxValue':
                validator = new MaxValueValidator(self.getOption('debug', false));
                break;
        }
        return validator;
    }
    this.getOption = this.getOption.bind(this);
}
/**
 * Returns a value of options, or default
 * @param name
 * @param defaultValue
 * @returns {*}
 */
ValidatorFactory.prototype.getOption = function(name, defaultValue) {
    return (this.options[name] && typeof this.options[name] != 'undefined') ? this.options[name] : defaultValue;
}

/**
 * Abstract validator
 * @param debug
 * @constructor
 */
function AbstractValidator(debug) {
    this.type = 'undefined';
    this.verbose = (debug === true);
}

/**
 * General validation, does not checks anything but logs in debug mode
 * @param value
 * @param params
 */
AbstractValidator.prototype.validate = function (value, params) {
    if (this.verbose === true) {
        console.log('Validation [' + this.type + '][' + value + '][' + params.join('|') + ']');
    }
}

/**
 * Required value validator
 * @param debug
 * @constructor
 */
function RequiredValidator(debug) {
    AbstractValidator.call(this, debug);

    this.type = 'required';
}
/**
 * It inherits AbstractValidator, but we override validation method
 * @type {RequiredValidator}
 */
RequiredValidator.prototype = Object.create(AbstractValidator.prototype);
RequiredValidator.prototype.validate = function (value, params) {
    AbstractValidator.prototype.validate.call(this, value, params);
    return (value && typeof value != 'undefined' && value != '') || false;
}

/**
 * Value is integer validator
 * @param debug
 * @constructor
 */
function IntegerValidator(debug) {
    AbstractValidator.call(this, debug);

    this.type = 'integer';
}
/**
 * It inherits AbstractValidator, but we override validation method
 * @type {IntegerValidator}
 */
IntegerValidator.prototype = Object.create(AbstractValidator.prototype);
IntegerValidator.prototype.validate = function (value, params) {
    AbstractValidator.prototype.validate.call(this, value, params);
    try {
        return (typeof parseInt(value) === 'number' && isFinite(value)) || false;
    } catch(e)
    {
        return false;
    }
}

/**
 * Minimum value validator
 * @param debug
 * @constructor
 */
function MinValueValidator(debug) {
    AbstractValidator.call(this, debug);

    this.type = 'minVal';
}

/**
 * It inherits AbstractValidator, but we override validation method
 * @type {MinValueValidator}
 */
MinValueValidator.prototype = Object.create(AbstractValidator.prototype);
MinValueValidator.prototype.validate = function (value, params) {
    AbstractValidator.prototype.validate.call(this, value, params);
    try {
        return (parseInt(value) >= params[0]) || false;
    } catch(e)
    {
        return false;
    }
}

/**
 * Maximum value validator
 * @param debug
 * @constructor
 */
function MaxValueValidator(debug) {
    AbstractValidator.call(this, debug);

    this.type = 'maxVal';
}

/**
 * It inherits AbstractValidator, but we override validation method
 * @type {MaxValueValidator}
 */
MaxValueValidator.prototype = Object.create(AbstractValidator.prototype);
MaxValueValidator.prototype.validate = function (value, params) {
    AbstractValidator.prototype.validate.call(this, value, params);
    try {
        return (parseInt(value) <= params[0]) || false;
    } catch(e)
    {
        return false;
    }
}

