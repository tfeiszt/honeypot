 
/**
 * Html control classes
 */
'use strict'

/**
 * Abstract control
 * @param options
 * @constructor
 */
function AbstractControl(options) {
    this.options = (options) ? options : {};
    this.type = 'undefined';
    /**
     * Returns a value of options object, or default
     * @param name
     * @param defaultValue
     * @returns {*}
     */
    this.getOption = function(name, defaultValue) {
        return (this.options[name] && typeof this.options[name] != 'undefined') ? this.options[name] : defaultValue;
    }
}

/**
 * General event, does not create anything but logs in debug mode
 */
AbstractControl.prototype.template = function() {
    if (this.getOption('debug', false) === true) {
        console.log(this.type + ' has been created');
    }
}

/**
 * Label
 * @param options
 * @constructor
 */
function Label(options) {
    AbstractControl.call(this, options);

    this.type = 'label';
}
/**
 * Label inherits AbstractControl, but we override template method to make a label
 * @type {Label}
 */
Label.prototype = Object.create(AbstractControl.prototype);
Label.prototype.template = function () {
    var control = document.createElement('label');
    control.innerHTML = this.getOption.call(this, 'caption', '');
    control.className = this.getOption.call(this, 'class', '');
    AbstractControl.prototype.template.call(this);
    return  control;
}

/**
 * Input
 * @param options
 * @constructor
 */
function Input(options) {
    AbstractControl.call(this, options);

    this.type = 'input';
}
/**
 * Input inherits AbstractControl, but we override template method to make a textbox
 * @type {Input}
 */
Input.prototype = Object.create(AbstractControl.prototype);
Input.prototype.template = function () {
    var control = document.createElement('input');
    control.className = this.getOption.call(this, 'class', '');
    AbstractControl.prototype.template.call(this);
    return  control;
}

/**
 * Button
 * @param options
 * @constructor
 */
function Button(options) {
    AbstractControl.call(this, options);

    this.type = 'button';
}
/**
 * Button inherits AbstractControl, but we override template method to make a button
 * @type {Button}
 */
Button.prototype = Object.create(AbstractControl.prototype);
Button.prototype.template = function () {
    var control = document.createElement('button');
    control.innerHTML = this.getOption.call(this, 'caption', 'Send');
    control.className = this.getOption.call(this, 'class', '');
    //Custom click event, or a default one
    control.addEventListener('click', this.getOption.call(this, 'onClick', function(event) {
        console.log('click');
    }));
    AbstractControl.prototype.template.call(this);
    return  control;
}


 
/**
 * Validator classes
 */
'use strict'

/**
 * ValidatorFactory
 * This class is able to create different kind of validator classes
 * @param options
 * @constructor
 */
function ValidatorFactory(options) {

    this.options = (options && typeof options != 'undefined') ? options : {};

    /**
     * Returns new validator instances
     * @param type
     * @returns {*}
     */
    this.getValidator = function( type ) {
        var self = this;
        var validator = null;
        //Registered validator classes
        //It would be nice to do dynamically by validatorClass.type or class name. Not big issue.
        switch (type) {
            case 'required':
                validator = new RequiredValidator(self.getOption('debug', false));
                break;
            case 'integer':
                validator = new IntegerValidator(self.getOption('debug', false));
                break;
            case 'minValue':
                validator = new MinValueValidator(self.getOption('debug', false));
                break;
            case 'maxValue':
                validator = new MaxValueValidator(self.getOption('debug', false));
                break;
        }
        return validator;
    }
    this.getOption = this.getOption.bind(this);
}
/**
 * Returns a value of options, or default
 * @param name
 * @param defaultValue
 * @returns {*}
 */
ValidatorFactory.prototype.getOption = function(name, defaultValue) {
    return (this.options[name] && typeof this.options[name] != 'undefined') ? this.options[name] : defaultValue;
}

/**
 * Abstract validator
 * @param debug
 * @constructor
 */
function AbstractValidator(debug) {
    this.type = 'undefined';
    this.verbose = (debug === true);
}

/**
 * General validation, does not checks anything but logs in debug mode
 * @param value
 * @param params
 */
AbstractValidator.prototype.validate = function (value, params) {
    if (this.verbose === true) {
        console.log('Validation [' + this.type + '][' + value + '][' + params.join('|') + ']');
    }
}

/**
 * Required value validator
 * @param debug
 * @constructor
 */
function RequiredValidator(debug) {
    AbstractValidator.call(this, debug);

    this.type = 'required';
}
/**
 * It inherits AbstractValidator, but we override validation method
 * @type {RequiredValidator}
 */
RequiredValidator.prototype = Object.create(AbstractValidator.prototype);
RequiredValidator.prototype.validate = function (value, params) {
    AbstractValidator.prototype.validate.call(this, value, params);
    return (value && typeof value != 'undefined' && value != '') || false;
}

/**
 * Value is integer validator
 * @param debug
 * @constructor
 */
function IntegerValidator(debug) {
    AbstractValidator.call(this, debug);

    this.type = 'integer';
}
/**
 * It inherits AbstractValidator, but we override validation method
 * @type {IntegerValidator}
 */
IntegerValidator.prototype = Object.create(AbstractValidator.prototype);
IntegerValidator.prototype.validate = function (value, params) {
    AbstractValidator.prototype.validate.call(this, value, params);
    try {
        return (typeof parseInt(value) === 'number' && isFinite(value)) || false;
    } catch(e)
    {
        return false;
    }
}

/**
 * Minimum value validator
 * @param debug
 * @constructor
 */
function MinValueValidator(debug) {
    AbstractValidator.call(this, debug);

    this.type = 'minVal';
}

/**
 * It inherits AbstractValidator, but we override validation method
 * @type {MinValueValidator}
 */
MinValueValidator.prototype = Object.create(AbstractValidator.prototype);
MinValueValidator.prototype.validate = function (value, params) {
    AbstractValidator.prototype.validate.call(this, value, params);
    try {
        return (parseInt(value) >= params[0]) || false;
    } catch(e)
    {
        return false;
    }
}

/**
 * Maximum value validator
 * @param debug
 * @constructor
 */
function MaxValueValidator(debug) {
    AbstractValidator.call(this, debug);

    this.type = 'maxVal';
}

/**
 * It inherits AbstractValidator, but we override validation method
 * @type {MaxValueValidator}
 */
MaxValueValidator.prototype = Object.create(AbstractValidator.prototype);
MaxValueValidator.prototype.validate = function (value, params) {
    AbstractValidator.prototype.validate.call(this, value, params);
    try {
        return (parseInt(value) <= params[0]) || false;
    } catch(e)
    {
        return false;
    }
}



 
/**
 * Controller class
 */
'use strict'

/**
 * General controller
 * It controls a view, has an input textbox and a button.
 * Handles custom validation on input, and custom callback using valid input.
 * @param options
 * @constructor
 */
function Controller(options) {
    this.options = (options && typeof options != 'undefined') ? options : {}; //custom options of behaviour
    this.view = null; //custom view object (object)
    this.rules = []; //validation rules (object array)
    this.validationMessages = []; //validation error messages (string array)
    this.callback = null; //custom callback (callable)
    this.getOption = this.getOption.bind(this);
    this.log = this.log.bind(this);
    this.setView = this.setView.bind(this);
    this.setCallback = this.setCallback.bind(this);
    this.setRules = this.setRules.bind(this);
    this.validate = this.validate.bind(this);
    this.doApplicationLogic = this.doApplicationLogic.bind(this);
}
/**
 * Returns a value of options, or default
 * @param name
 * @param defaultValue
 * @returns {*}
 */
Controller.prototype.getOption = function(name, defaultValue) {
    return (this.options[name] && typeof this.options[name] != 'undefined') ? this.options[name] : defaultValue;
}

/**
 * Output to console
 * @param message
 */
Controller.prototype.log = function(message) {
    console.log(message);
}

/**
 * Delegates a view
 * @param view
 * @returns {Controller}
 */
Controller.prototype.setView = function (view) {
    this.view = view;
    return this;
}

/**
 * Delegates a callback
 * @param callback
 * @returns {Controller}
 */
Controller.prototype.setCallback = function(callback) {
    this.callback = callback;
    return this;
}

/**
 * Delegates a rule array
 * @param rules
 * @returns {Controller}
 */
Controller.prototype.setRules = function(rules) {
    this.rules = rules;
    return this;
}

/**
 * Validation
 * It creates a ValidationFactory instance which handles registered validation processes.
 * @param value
 * @returns {boolean}
 */
Controller.prototype.validate = function(value) {
    var self = this;
    var vFactory = new ValidatorFactory({
        'debug' : this.getOption('debug', false)
    });
    this.validationMessages = [];
    this.rules.every(function(item){

        //creates a Validator instance by type
        var validator = vFactory.getValidator(item.type);

        //runs validation on value, and optionally compares to expected values
        if (! validator.validate(value, item.params)) {
            self.validationMessages.push(item.message);
            return false;  // Terminate validation at first failure
        } else {
            return true;
        }
    });
    return (this.validationMessages.length === 0) || false;
}

/**
 * General application logic: Run validation, if passed, run custom callback.
 * @param event
 */
Controller.prototype.doApplicationLogic = function(event) {
    // get input value
    // Honestly that would be nice if this value could be comes via an onChange event listener...
    var value = (event.target.parentNode.getElementsByTagName('input')[0]).value;
    // As view is an object all callable on that must be tested.
    // It would be also nice to use interface which is not supported in native ES5 (as my knowledge)
    if (typeof(this.view.hideError) == "function") {
        this.view.hideError();
    }
    if (! this.validate(value)) {
        //Same check like above.
        if (typeof(this.view.showError) == "function") {
            //show validation message
            this.view.showError(this.validationMessages.join('<br>'));
        } else {
            this.log(this.validationMessages.join(', '));
        }
    } else {
        //Passed, do custom application logic
        this.callback(value);
    }
}



/**
 * Application
 * ES5 test
 */
'use strict'

var app = app || {}
/**
 * Options
 * @type {{debug: boolean}}
 */
app.options = {
    'debug': false
};
/**
 * Returns container <div>
 * @returns {Element}
 */
app.getContainer = function() {
    return document.getElementById('honeypot');
}
/**
 * Create controller
 * @type {Controller}
 */
app.controller = new Controller(app.options);
/**
 * Set up validation rules
 */
app.controller.setRules([
    {'type': 'required', 'message': 'Field is mandatory', 'params': []},
    {'type': 'integer', 'message': 'It must be a number', 'params': []},
    {'type': 'minValue', 'message': 'Minimum value is 0', 'params': [0]},
    {'type': 'maxValue', 'message': 'Maximum value is 10k', 'params': [10000]}
]);
/**
 * Set up a general view object
 */
app.controller.setView({
    /**
     * Rendering following html elements:
     * label for caption
     * texbox
     * button
     * label for error messages
     */
    render: function() {
        app.getContainer().appendChild(
            (new Label({
                'caption': 'Number of bees: ',
                'class': '',
                'debug': app.options.debug
                })
            ).template()
        ).parentNode.appendChild(
            (new Input({
                'debug': app.options.debug
                })
            ).template()
        ).parentNode.appendChild(
            (new Button({
                    'onClick': app.controller.doApplicationLogic,
                    'debug': app.options.debug
                })
            ).template()
        ).parentNode.appendChild(
            (new Label({
                    'caption': '',
                    'class': 'hidden',
                    'debug': app.options.debug
                })
            ).template()
        );
    },

    /**
     * Shows an error message
     * @param message
     */
    showError: function(message) {
        app.getContainer().lastElementChild.innerHTML = message;
        app.getContainer().lastElementChild.className = 'error';
    },

    /**
     * Hide error message
     */
    hideError: function() {
        app.getContainer().lastElementChild.innerHTML = '';
        app.getContainer().lastElementChild.className = 'hidden';
    }
});
/**
 * Custom callback on button click
 */
app.controller.setCallback(function(value){
    var v = parseInt(value);
    for(var i = 0; i < v; i++) {
        var delay = 20 * i;
        setTimeout(app.controller.log, delay, i);
    }
    setTimeout(app.controller.log, (20 * v), value + ' bee(s) in the honeypot');
});
/**
 * Render application view
 */
app.controller.view.render();
