# tfeiszt honeypot  

### Description
ES5 test application
 
### Browser compatibility
   * IE9
   * IE10
   * IE11
   * Firefox
   * Safari
   * Opera
   * Chrome
   * Edge

### Pre-processors
  * Sass
  
### Compiler
  * Grunt
  
### Installing  

```
$ npm install
$ grunt
```
  
### Grunt setting to build production
See option "devMode" in package.json. If this option is false then css and js files are minimized, otherwise not. Default option is true.
```
"devMode": false,
```

### Optional settings
See app.options in app.js.
```
app.options = {
    'debug': false //Set this true to verbose mode. Default: false
};
```
 
### License

MIT
